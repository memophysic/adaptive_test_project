from time import sleep

import untangle

from adaptive import AdaptiveMutableSubstitute
from monitor.observer_pattern import Observer
from monitor.parameter import DiscreteParameter
from monitor.polling_dynamic_monitor import PollingDynamicMonitorDecorator
from monitor.primitives import Monitor, MutableMonitorsProvider, DynamicMonitor


class MissingWeatherFile(Exception):
    def __init__(self, msg):
        self._msg = msg

    def __str__(self):
        return str(self._msg)


class Weather(DiscreteParameter):
    sunny = 0
    raining = 1
    snowing = 2


class WeatherMonitor(Monitor):
    def __init__(self, file):
        self._file = file
        self.possible_values = Weather.possible_values

    def value(self):
        """
        Acquire the value from a file
        """
        cfg = untangle.parse(self._file)
        if cfg is None:
            raise MissingWeatherFile(
                "Expected configuration file at '{}', but it is "
                "missing".format(self._file))

        val = {
            "sunny": Weather.sunny,
            "raining": Weather.raining,
            "snowing": Weather.snowing}.get(cfg.weather.cdata.lower(), None)
        ret_val = val or Weather.sunny  # val from file or default = sunny

        # Post-condition = Value is one of possible_values
        assert(ret_val in self.possible_values())
        return ret_val


class Umbrella(AdaptiveMutableSubstitute, Observer):
    @classmethod
    def defined_context(cls):
        return {
            Weather: Weather.get_range(0, 1)
        }

    def __init__(self, monitors_provider):
        """
        :type monitors_provider: MutableMonitorsProvider
        """
        super().__init__(monitors_provider)

        self._opened = False  # Default value

        # Request and register to dynamic monitors
        self._request_dynamic_monitor_or_decorate(
            Weather,
            PollingDynamicMonitorDecorator
        ).register_monitor(observer=self)

    def prune(self):
        super().prune()
        self._request_dynamic_monitor(Weather).unregister(self)

    def _adapt_to_weather(self, value):
        print("The umbrella was: {}".format("opened" if self._opened else
                                            "closed"))
        self._opened = value is Weather.raining
        print("The umbrella is now: {}".format("opened" if self._opened else
                                               "closed"))

    def observed_update(self, observable, value, **kwarg):
        {
            Weather: lambda v: self._adapt_to_weather(v)
        }.get(value.__class__)(value)


if __name__ == "__main__":
    # Static monitor
    weatherMtr = WeatherMonitor(
        """<?xml version="1.0"?>
        <weather>Raining</weather>
        """)
    print("Static monitor value: {}".format(weatherMtr.value().name))

    # Monitor on external file
    weatherStaticMtr = WeatherMonitor("weather.xml")

    # Registered component updated upon change to weather monitor value
    class DummyMonitorsProvider(MutableMonitorsProvider):
        def __init__(self):
            self._monitors = {
                Weather: weatherStaticMtr
            }

        def replace_monitor(self, parameter, new_monitor):
            self._monitors[parameter] = new_monitor

        def request_monitor(self, parameter):
            return self._monitors.get(parameter, None)

    monitorsPrv = DummyMonitorsProvider()
    umbrellaCmp = Umbrella(monitorsPrv)
    umbrellaCmp2 = Umbrella(monitorsPrv)

    sleep(2)
    umbrellaCmp.prune()

    sleep(2)
    umbrellaCmp2.prune()


