from adaptivepy.component.adaptation_space import AdaptationSpace, \
    filter_by_adaptation_space
from adaptivepy.component.adaptive import Adaptive
from adaptivepy.component.adaptive_proxyrouter import \
    AdaptiveInternalProxyRouter, AdaptiveProxyRouter
from adaptivepy.monitor.base_implementations import create_random_monitor, \
    create_fixed_monitor
from adaptivepy.monitor.monitor_event_manager import MonitorEventManager
from adaptivepy.monitor.parameter import EnumeratedParameter

# Parameter
from adaptivepy.monitor.pull_dynamic_monitor import PullDynamicMonitorDecorator
from adaptivepy.state_space.enumerated_state_space import \
    EnumeratedStateSpace as ESS


class AnimalSize(EnumeratedParameter):
    small = 0
    medium = 1
    large = 2
    gigantic = 3


class Weather(EnumeratedParameter):
    sunny = 0
    raining = 1
    snowing = 2

# Monitors
monitorsMng = MonitorEventManager()

randomAnimalSize = PullDynamicMonitorDecorator(
    create_random_monitor(AnimalSize.subset(1, 3)))
    # create_fixed_monitor(AnimalSize.gigantic))

monitorsMng.register_monitor(AnimalSize, randomAnimalSize)

randomWeather = PullDynamicMonitorDecorator(create_random_monitor(
    Weather.possible_values()))
monitorsMng.register_monitor(Weather, randomWeather)


# Components
class AnimalContainer:
    """
    Interface for an animal container which can hold one animal and various
    tasks which can be done on it
    """
    def set_animal(self, animal):
        raise NotImplementedError()

    def has_animal(self):
        raise NotImplementedError()

    def clean(self):
        raise NotImplementedError()


class CompositeAnimalContainer(AdaptiveInternalProxyRouter):
    """
    Abstract composite component which can be either a cage or cockpit
    """

    KEY_VAR1 = "var1"
    KEY_VAR2 = "var2"

    def __init__(self, var1=None, var2=None):
        AnimalContainer.__init__(self)
        AdaptiveInternalProxyRouter.__init__(
            self, parameter_value_provider=monitorsMng)

        self._arguments_dict = {
            self.KEY_VAR1: var1,
            self.KEY_VAR2: var2
        }

        self._subscribe_to_all_parameters()
        self.route(self.choose_route())

    @classmethod
    def candidates(cls, arguments_provider=None):
        arguments_provider = arguments_provider or {}
        param_val_prv = arguments_provider.get("parameter_value_provider", None)

        return {
            Cage: lambda: Cage(arguments_provider[cls.KEY_VAR1],
                               arguments_provider[cls.KEY_VAR2],
                               param_val_prv),
            Cockpit: lambda: Cockpit((arguments_provider[cls.KEY_VAR1],
                                      arguments_provider[cls.KEY_VAR2]),
                                     param_val_prv)
        }

    def arguments_provider(self):
        arg_prv = AdaptiveProxyRouter.arguments_provider(self)
        """:type: dict[str, Any]"""
        arg_prv.update(self._arguments_dict)
        return arg_prv

    def choose_route(self):
        # Dummy for test
        snapshot = self.local_snapshot()
        valid_options = filter_by_adaptation_space(self.candidates(),
                                                   snapshot)
        if not valid_options:
            raise RuntimeError("Unhandled adaptation state!")

        # Choose first for dummy example
        return next(iter(valid_options))


@AdaptationSpace({AnimalSize: AnimalSize.subset(0, 2),
                  Weather: Weather.subset(0, 1)})
class Cage(Adaptive, AnimalContainer):

    def __init__(self, varX, varY, parameter_value_provider=None):
        AnimalContainer.__init__(self)
        Adaptive.__init__(self, parameter_value_provider)
        self._animal = None
        self._varX = None or varX
        self._varY = None or varY

    def has_animal(self):
        return self._animal is not None

    def clean(self):
        print("{}: The cage is being cleaned, elapsed time: 30 minutes.\n"
              "Validate data: {}, {}".format(self.__class__.__name__,
                                             self._varX,
                                             self._varY))

    def set_animal(self, animal):
        print("{}: Setting animal {} in the cage".format(
            self.__class__.__name__,
            str(animal)))
        self._animal = animal

    def updated_monitored_value(self, parameter, old_value, new_value):
        pass


@AdaptationSpace({AnimalSize: AnimalSize.subset(2, 3)})
class Cockpit(Adaptive, AnimalContainer):

    def __init__(self, var, parameter_value_provider=None):
        AnimalContainer.__init__(self)
        Adaptive.__init__(self, parameter_value_provider)
        self._animal = None
        self._varX = var[0]
        self._varY = var[1]

    def has_animal(self):
        return self._animal is not None

    def clean(self):
        print("{}: The cockpit is being cleaned, elapsed time: 2 hours\n"
              "Validate data: {}, {}".format(self.__class__.__name__,
                                             self._varX,
                                             self._varY))

    def set_animal(self, animal):
        print("{}: Setting animal {} in the cockpit".format(
            self.__class__.__name__,
            str(animal)))
        self._animal = animal

    def updated_monitored_value(self, parameter, old_value, new_value):
        pass


if __name__ == "__main__":
    print("Context:\n"
          "\tAnimalSize: {}\n"
          "\tWeather: {}".format(AnimalSize(randomAnimalSize.value()).name,
                                 Weather(randomWeather.value()).name))

    # Test program
    container = CompositeAnimalContainer(1, 2)
    """:type: AnimalContainer"""

    print("Is there an animal? {}".format(
        "yes" if container.has_animal() else "no"))
    cat = "Cat"
    print("Adding {}".format(cat))
    container.set_animal(cat)
    print("Is there an animal? {}".format(
        "yes" if container.has_animal() else "no"))
    container.clean()
