from adaptive import Adaptive, LazyAdaptiveProxyRouter


class CommonInterface:
    def do_something(self, var):
        raise NotImplementedError()


class Option1(CommonInterface, Adaptive):
    @classmethod
    def defined_context(cls):
        return {"Number": {1, 2}}

    def do_something(self, var):
        print("Option1.doSomething: {}".format(str(var)))


class Option2(CommonInterface, Adaptive):
    @classmethod
    def defined_context(cls):
        return {"Number": {3, 4},
                "Direction": {"Left", "Right"}}

    def do_something(self, var):
        print("Option2.doSomething: {}".format(str(var)))


# Static composite
class BaseOptionContainer(CommonInterface, LazyAdaptiveProxyRouter):
    @classmethod
    def substitution_candidates(cls, arguments_provider=None):
        return {
            Option1: lambda: Option1(),
            Option2: lambda: Option2()
        }

    def __init__(self):
        LazyAdaptiveProxyRouter.__init__(self, locals())

    def _route_to_substitute(self):
        self._delegate_methods_to_instance(CommonInterface)


class MethodSolverOptionContainer(BaseOptionContainer):
    def _choose_substitute(self):
        # Logic here based on monitors, here we route r
        for o in self.substitution_candidates():
            if o.__name__ == "Option1":
                return o


class ExternalSolverOptionContainer(BaseOptionContainer):
    def __init__(self, solver):
        self.choose_substitute = lambda: solver(
            self.substitution_candidates(),
            {}
        )
        super().__init__()

print(Option1.defined_context())
print(Option2.defined_context())

print(MethodSolverOptionContainer.defined_context())
composite = MethodSolverOptionContainer()
composite.do_something("Called from MethodSolverOptionContainer")


def dummy_solver(options, monitors):
    for o in options:
        if o.__name__ == "Option2":
            return o

composite2 = ExternalSolverOptionContainer(dummy_solver)
composite2.do_something("Called from ExternalSolverOptionContainer")

# Dynamic composite
# class DynamicOptionContainer(DynamicAdaptiveComposite):
#     pass
#
# DynamicOptionContainer.add_substitution_candidate(Option1)
# DynamicOptionContainer.add_substitution_candidate(Option2)
#
# print(DynamicOptionContainer.defined_context())
