class AnimalContainer:
    """
    Interface for an animal container which can hold one animal and various
    tasks which can be done on it
    """
    def set_animal(self, animal):
        raise NotImplementedError()

    def has_animal(self):
        raise NotImplementedError()

    def clean(self):
        raise NotImplementedError()