from adaptivepy.monitor.parameter import EnumeratedParameter, DiscreteParameter


class AnimalSize(EnumeratedParameter):
    small = 0
    medium = 1
    large = 2
    gigantic = 3

Temperature = DiscreteParameter(-40, 60, 0.5)
