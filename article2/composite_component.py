from adaptivepy.component.adaptation_space import filter_by_adaptation_space
from adaptivepy.component.adaptive_proxyrouter import \
    AdaptiveInternalProxyRouter, AdaptiveProxyRouter
from article2.animal_container import AnimalContainer
from article2.primitive_components import Cage, Barn, VirtualEcosystem, \
    HeatedArea, JumboCage


class CompositeAnimalContainer(AdaptiveInternalProxyRouter):
    """
    Composite component which can be a cage, barn or a virtual ecosystem
    """

    def __init__(self, parameter_value_provider=None):
        AnimalContainer.__init__(self)
        AdaptiveInternalProxyRouter.__init__(
            self, parameter_value_provider=parameter_value_provider)

        self._subscribe_to_all_parameters()
        self.route(self.choose_route())

    @classmethod
    def candidates(cls, arguments_provider=None):
        arguments_provider = arguments_provider or {}
        param_val_prv = arguments_provider.get("parameter_value_provider", None)

        return {
            Cage: lambda: Cage(param_val_prv),
            Barn: lambda: Barn(param_val_prv),
            VirtualEcosystem: lambda: VirtualEcosystem(param_val_prv),
        }

    def choose_route(self):
        # Dummy for test
        snapshot = self.local_snapshot()
        valid_options = filter_by_adaptation_space(self.candidates(),
                                                   snapshot)
        if not valid_options:
            raise RuntimeError("Unhandled adaptation state!")

        # Choose first for dummy example
        return next(iter(valid_options))


class ExtendedCompositeAnimalContainer(CompositeAnimalContainer):
    def __init__(self, parameter_value_provider=None):
        super().__init__(parameter_value_provider=parameter_value_provider)

    @classmethod
    def candidates(self, arguments_provider=None):
        candidates = super().candidates(arguments_provider=arguments_provider)
        arguments_provider = arguments_provider or {}
        param_val_prv = arguments_provider.get("parameter_value_provider", None)

        candidates.update({HeatedArea: lambda: HeatedArea(param_val_prv)})
        return candidates


class JumboExtendedCompositeAnimalContainer(CompositeAnimalContainer):
    def __init__(self, parameter_value_provider=None):
        super().__init__(parameter_value_provider=parameter_value_provider)

    @classmethod
    def candidates(self, arguments_provider=None):
        candidates = super().candidates(arguments_provider=arguments_provider)
        arguments_provider = arguments_provider or {}
        param_val_prv = arguments_provider.get("parameter_value_provider", None)

        candidates.pop(Cage)
        candidates.update({JumboCage: lambda: JumboCage(param_val_prv)})
        return candidates
