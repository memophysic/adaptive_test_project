from adaptivepy.component.adaptation_space import AdaptationSpace, \
    extend_adaptation_space
from adaptivepy.component.adaptive import Adaptive
from adaptivepy.state_space.discrete_state_space import \
    DiscreteStateSpace as Dss
from adaptivepy.state_space.enumerated_state_space import \
    EnumeratedStateSpace as Ess
from article2.animal_container import AnimalContainer
from article2.parameters import AnimalSize, Temperature


@AdaptationSpace({AnimalSize: AnimalSize.subset(0, 1),
                  Temperature: Dss(10, 60, 0.5)})
class Cage(Adaptive, AnimalContainer):
    def __init__(self, parameter_value_provider=None):
        AnimalContainer.__init__(self)
        Adaptive.__init__(self, parameter_value_provider)
        self._animal = None

    def has_animal(self):
        return self._animal is not None

    def clean(self):
        print("{}: The cage is being cleaned, elapsed time: 30 minutes".format(
            self.__class__.__name__))

    def set_animal(self, animal):
        print("{}: Setting animal {} in the cage".format(
            self.__class__.__name__,
            str(animal)))
        self._animal = animal

    def updated_monitored_value(self, parameter, old_value, new_value):
        pass


class JumboCage(Cage):
    def __init__(self, parameter_value_provider=None):
        super().__init__(parameter_value_provider=parameter_value_provider)

    @classmethod
    def adaptation_space(cls):
        return extend_adaptation_space(Cage.adaptation_space().copy(),
                                       AnimalSize,
                                       {AnimalSize.large})


@AdaptationSpace({AnimalSize: AnimalSize.subset(0, 2),
                  Temperature: Dss(-40, 30, 0.5)})
class Barn(Adaptive, AnimalContainer):
    def __init__(self, parameter_value_provider=None):
        AnimalContainer.__init__(self)
        Adaptive.__init__(self, parameter_value_provider)
        self._animal = None

    def has_animal(self):
        return self._animal is not None

    def clean(self):
        print("{}: The barn is being cleaned, elapsed time: 1.5 "
              "hours".format(self.__class__.__name__))

    def set_animal(self, animal):
        print("{}: Setting animal {} in the barn".format(
            self.__class__.__name__,
            str(animal)))
        self._animal = animal

    def updated_monitored_value(self, parameter, old_value, new_value):
        pass


@AdaptationSpace({AnimalSize: Ess({AnimalSize.gigantic})})
class VirtualEcosystem(Adaptive, AnimalContainer):
    def __init__(self, parameter_value_provider=None):
        AnimalContainer.__init__(self)
        Adaptive.__init__(self, parameter_value_provider)
        self._animal = None

    def has_animal(self):
        return self._animal is not None

    def clean(self):
        print("{}: The virtual ecosystem is being cleaned, elapsed time: 4 "
              "hours".format(self.__class__.__name__))

    def set_animal(self, animal):
        print("{}: Setting animal {} in the virtual ecosystem".format(
            self.__class__.__name__,
            str(animal)))
        self._animal = animal

    def updated_monitored_value(self, parameter, old_value, new_value):
        pass


@AdaptationSpace({AnimalSize: AnimalSize.subset(2, 3),
                  Temperature: Dss(20, 60, 0.5)})
class HeatedArea(Adaptive, AnimalContainer):
    def __init__(self, parameter_value_provider=None):
        AnimalContainer.__init__(self)
        Adaptive.__init__(self, parameter_value_provider)
        self._animal = None

    def has_animal(self):
        return self._animal is not None

    def clean(self):
        print("{}: The heated area is being cleaned, elapsed time: 3.5 "
              "hours".format(self.__class__.__name__))

    def set_animal(self, animal):
        print("{}: Setting animal {} in the heated area".format(
            self.__class__.__name__,
            str(animal)))
        self._animal = animal

    def updated_monitored_value(self, parameter, old_value, new_value):
        pass