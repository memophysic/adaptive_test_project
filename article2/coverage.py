from adaptivepy.monitor.base_implementations import create_fixed_monitor, \
    create_random_monitor
from adaptivepy.monitor.monitor_event_manager import MonitorEventManager
from adaptivepy.monitor.pull_dynamic_monitor import PullDynamicMonitorDecorator
from adaptivepy.state_space.discrete_state_space import DiscreteStateSpace
from adaptivepymetrics.adaptation_space_coverage import \
    compute_component_coverage
from article2.composite_component import CompositeAnimalContainer, \
    ExtendedCompositeAnimalContainer, JumboExtendedCompositeAnimalContainer
from article2.parameters import AnimalSize, Temperature
from article2.primitive_components import Cage, Barn, VirtualEcosystem, \
    HeatedArea, JumboCage


def print_coverage(adaptive_component_cls, adaptive_component_factory):
    """
    :type adaptive_component_cls: cls
    :type adaptive_component_factory: () -> Adaptive
    """
    print("Modelled coverage {}: {:5.4f}%".format(
        adaptive_component_cls,
        compute_component_coverage(adaptive_component_cls) * 100))

    adaptive_component_instance = adaptive_component_factory()
    print("Effective coverage {}: {:5.4f}%".format(
        adaptive_component_instance,
        compute_component_coverage(adaptive_component_instance,
                                   effective_coverage=True) * 100))


if __name__ == "__main__":
    monitorsMng = MonitorEventManager()
    # AnimalSize monitor
    animalSizeMonitor = PullDynamicMonitorDecorator(
        create_random_monitor(AnimalSize.possible_values()))
    monitorsMng.register_monitor(AnimalSize, animalSizeMonitor)

    # Temperature monitor
    temperatureMonitor = PullDynamicMonitorDecorator(
        create_random_monitor(Temperature.possible_values()))
        # create_random_monitor(DiscreteStateSpace(-40, 30, 0.5)))
    monitorsMng.register_monitor(Temperature, temperatureMonitor)

    for component in [Cage, Barn, VirtualEcosystem, HeatedArea, JumboCage,
                      CompositeAnimalContainer,
                      ExtendedCompositeAnimalContainer,
                      JumboExtendedCompositeAnimalContainer]:
        print_coverage(component,
                       lambda: component(parameter_value_provider=monitorsMng))

