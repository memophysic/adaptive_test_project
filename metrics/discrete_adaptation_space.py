from adaptivepy.component.adaptation_space import AdaptationSpace, \
    filter_by_adaptation_space
from adaptivepy.component.adaptive import Adaptive
from adaptivepy.component.adaptive_proxyrouter import \
    AdaptiveInternalProxyRouter, AdaptiveProxyRouter
from adaptivepy.monitor.base_implementations import create_random_monitor, \
    create_fixed_monitor
from adaptivepy.monitor.monitor_event_manager import MonitorEventManager
from adaptivepy.monitor.parameter import DiscreteParameter
from adaptivepy.monitor.pull_dynamic_monitor import PullDynamicMonitorDecorator
from adaptivepy.state_space.discrete_state_space import \
    DiscreteStateSpace as Dss
from metrics.print_coverage import print_coverage


class AnimalContainer:
    """
    Interface for an animal container which can hold one animal and various
    tasks which can be done on it
    """
    def set_animal(self, animal):
        raise NotImplementedError()

    def has_animal(self):
        raise NotImplementedError()

    def clean(self):
        raise NotImplementedError()

# == Parameters
# Approximated animal longest size in meters (+/- 0.5 m)
animalSize = DiscreteParameter(min_value=0.5, max_value=10, step=0.5)
# Approximated temperature (+/- 1 degree Celsius)
weather = DiscreteParameter(min_value=-40, max_value=80, step=1)

# == Monitors
monitorsMng = MonitorEventManager()

animalSizeMonitor = PullDynamicMonitorDecorator(
    create_fixed_monitor(5))
    # create_random_monitor(animalSize.possible_values()))
monitorsMng.register_monitor(animalSize, animalSizeMonitor)

weatherMonitor = PullDynamicMonitorDecorator(
    # create_fixed_monitor(20))
    create_random_monitor(Dss(0, 80, 1)))
monitorsMng.register_monitor(weather, weatherMonitor)


@AdaptationSpace({animalSize: Dss(1, 6, 0.5),
                  weather: Dss(10, 80, 1)})
class Cage(Adaptive, AnimalContainer):

    def __init__(self, varX, varY, parameter_value_provider=None):
        AnimalContainer.__init__(self)
        Adaptive.__init__(self, parameter_value_provider)
        self._animal = None
        self._varX = None or varX
        self._varY = None or varY

    def has_animal(self):
        return self._animal is not None

    def clean(self):
        print("{}: The cage is being cleaned, elapsed time: 30 minutes.\n"
              "Validate data: {}, {}".format(self.__class__.__name__,
                                             self._varX,
                                             self._varY))

    def set_animal(self, animal):
        print("{}: Setting animal {} in the cage".format(
            self.__class__.__name__,
            str(animal)))
        self._animal = animal

    def updated_monitored_value(self, parameter, old_value, new_value):
        pass


@AdaptationSpace({animalSize: Dss(4, 10, 0.5)})
class Cockpit(Adaptive, AnimalContainer):

    def __init__(self, var, parameter_value_provider=None):
        AnimalContainer.__init__(self)
        Adaptive.__init__(self, parameter_value_provider)
        self._animal = None
        self._varX = var[0]
        self._varY = var[1]

    def has_animal(self):
        return self._animal is not None

    def clean(self):
        print("{}: The cockpit is being cleaned, elapsed time: 2 hours\n"
              "Validate data: {}, {}".format(self.__class__.__name__,
                                             self._varX,
                                             self._varY))

    def set_animal(self, animal):
        print("{}: Setting animal {} in the cockpit".format(
            self.__class__.__name__,
            str(animal)))
        self._animal = animal

    def updated_monitored_value(self, parameter, old_value, new_value):
        pass


class CompositeAnimalContainer(AdaptiveInternalProxyRouter):
    """
    Abstract composite component which can be either a cage or cockpit
    """

    KEY_VAR1 = "var1"
    KEY_VAR2 = "var2"

    def __init__(self, var1=None, var2=None):
        AnimalContainer.__init__(self)
        AdaptiveInternalProxyRouter.__init__(
            self, parameter_value_provider=monitorsMng)

        self._arguments_dict = {
            self.KEY_VAR1: var1,
            self.KEY_VAR2: var2
        }

        self._subscribe_to_all_parameters()
        self.route(self.choose_route())

    @classmethod
    def candidates(cls, arguments_provider=None):
        arguments_provider = arguments_provider or {}
        param_val_prv = arguments_provider.get("parameter_value_provider", None)

        return {
            Cage: lambda: Cage(arguments_provider[cls.KEY_VAR1],
                               arguments_provider[cls.KEY_VAR2],
                               param_val_prv),
            Cockpit: lambda: Cockpit((arguments_provider[cls.KEY_VAR1],
                                      arguments_provider[cls.KEY_VAR2]),
                                     param_val_prv)
        }

    def arguments_provider(self):
        arg_prv = AdaptiveProxyRouter.arguments_provider(self)
        """:type: dict[str, Any]"""
        arg_prv.update(self._arguments_dict)
        return arg_prv

    def choose_route(self):
        # Dummy for test
        snapshot = self.local_snapshot()
        valid_options = filter_by_adaptation_space(self.candidates(),
                                                   snapshot)
        if not valid_options:
            raise RuntimeError("Unhandled adaptation state!")

        # Choose first for dummy example
        return next(iter(valid_options))


if __name__ == "__main__":
    # == Coverage
    # print_coverage(Cage, lambda: Cage(0, 0, monitorsMng))
    # print_coverage(Cockpit, lambda: Cockpit((0, 0), monitorsMng))
    print_coverage(CompositeAnimalContainer,
                   lambda: CompositeAnimalContainer(0, 0))

    # == Adaptation test program
    # print("Context:\n"
    #       "\tAnimalSize: {}\n"
    #       "\tWeather: {}".format(animalSizeMonitor.value(),
    #                              weatherMonitor.value()))
    #
    # # Test program
    # container = CompositeAnimalContainer(1, 2)
    # """:type: AnimalContainer"""
    #
    # print("Is there an animal? {}".format(
    #     "yes" if container.has_animal() else "no"))
    # cat = "Cat"
    # print("Adding {}".format(cat))
    # container.set_animal(cat)
    # print("Is there an animal? {}".format(
    #     "yes" if container.has_animal() else "no"))
    # container.clean()
