from adaptivepy.component.adaptive import Adaptive
from adaptivepymetrics.adaptation_space_coverage import \
    compute_component_coverage


def print_coverage(adaptive_component_cls, adaptive_component_factory,
                   fcoverage=compute_component_coverage):
    """
    :type adaptive_component_cls: cls
    :type adaptive_component_factory: () -> Adaptive
    :param fcoverage: () -> float
    """
    print("Modelled coverage {}: {:5.4f}%".format(
        adaptive_component_cls,
        fcoverage(adaptive_component_cls) * 100))

    # adaptive_component_instance = adaptive_component_factory()
    # print("Effective coverage {}: {:5.4f}%".format(
    #     adaptive_component_instance,
    #     fcoverage(adaptive_component_instance, effective_coverage=True) * 100))