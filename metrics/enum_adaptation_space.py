from adaptivepy.component.adaptation_space import AdaptationSpace
from adaptivepy.component.adaptive import Adaptive
from adaptivepy.state_space.enumerated_state_space import EnumeratedStateSpace
from metrics.print_coverage import print_coverage
from substitution_example import Cage, Cockpit, CompositeAnimalContainer, \
    monitorsMng, AnimalContainer, AnimalSize, Weather


@AdaptationSpace({AnimalSize: AnimalSize.subset(1, 3),
                  Weather: Weather.subset(1, 2)})
class Hangar(Adaptive, AnimalContainer):

    def __init__(self, parameter_value_provider=None):
        AnimalContainer.__init__(self)
        Adaptive.__init__(self, parameter_value_provider)
        self._animal = None

    def has_animal(self):
        return self._animal is not None

    def clean(self):
        print("{}: The hangar is being cleaned, elapsed time: 1.5 hours\n"
              "Validate data: N/A".format(self.__class__.__name__))

    def set_animal(self, animal):
        print("{}: Setting animal {} in the hangar".format(
            self.__class__.__name__,
            str(animal)))
        self._animal = animal

    def updated_monitored_value(self, parameter, old_value, new_value):
        pass


@AdaptationSpace({AnimalSize: EnumeratedStateSpace({AnimalSize(2)}),
                  Weather: EnumeratedStateSpace({Weather(1)})})
class SpecialHabitacle(Adaptive, AnimalContainer):

    def __init__(self, parameter_value_provider=None):
        AnimalContainer.__init__(self)
        Adaptive.__init__(self, parameter_value_provider)
        self._animal = None

    def has_animal(self):
        return self._animal is not None

    def clean(self):
        print("{}: The special habitable is being cleaned, elapsed time: 0.75 "
              "hours\n"
              "Validate data: N/A".format(self.__class__.__name__))

    def set_animal(self, animal):
        print("{}: Setting animal {} in the special habitacle".format(
            self.__class__.__name__,
            str(animal)))
        self._animal = animal

    def updated_monitored_value(self, parameter, old_value, new_value):
        pass


class BetterCompositeAnimalContainer(CompositeAnimalContainer):

    @classmethod
    def candidates(cls, arguments_provider=None):
        candidates = CompositeAnimalContainer.candidates(arguments_provider)
        arguments_provider = arguments_provider or {}
        param_val_prv = arguments_provider.get("parameter_value_provider", None)

        candidates[Hangar] = lambda: Hangar(param_val_prv)
        candidates[SpecialHabitacle] = lambda: SpecialHabitacle(param_val_prv)
        return candidates


# print_coverage(Cage, lambda: Cage(0, 0, monitorsMng))
# print_coverage(Cockpit, lambda: Cockpit((0, 0), monitorsMng))
# print_coverage(Hangar, lambda: Hangar(monitorsMng))

print_coverage(CompositeAnimalContainer,
               lambda: CompositeAnimalContainer(0, 0))

print_coverage(BetterCompositeAnimalContainer,
               lambda: BetterCompositeAnimalContainer(0, 0))