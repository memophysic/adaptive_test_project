from adaptive import LazyAdaptiveProxyRouter, Adaptive, add_to_context, AdaptiveSubstitute, filter_substitutes
from monitor.base_implementations import FixedMonitor
from monitor.manager.monitors_manager import MonitorsManager
from monitor.parameter import DiscreteParameter


class ConnectionStatus(DiscreteParameter):
    offline = 0
    online = 1
    connecting = 2
    disconnecting = 3


class FormCompleted(DiscreteParameter):
    complete = 0
    incomplete = 1
    complete_with_error = 2

manager = MonitorsManager()
manager.register(FormCompleted, FixedMonitor(FormCompleted.complete))
manager.register(ConnectionStatus, FixedMonitor(ConnectionStatus.offline))


class SomeLayout(Adaptive):
    @classmethod
    def defined_context(cls):
        return {FormCompleted: {FormCompleted.complete}}

    def monitors_provider(self):
        return manager


class SomeLabel(Adaptive):
    @classmethod
    def defined_context(cls):
        return {ConnectionStatus: ConnectionStatus.get_range(0, 1)}

    def monitors_provider(self):
        return manager


class SaveButton(AdaptiveSubstitute):
    @classmethod
    def defined_context(cls):
        return {ConnectionStatus: ConnectionStatus.offline}

    def monitors_provider(self):
        return manager


class SendButton(AdaptiveSubstitute):
    @classmethod
    def defined_context(cls):
        return {ConnectionStatus: {ConnectionStatus.online}}


class PlaceRequestButton(AdaptiveSubstitute):
    @classmethod
    def defined_context(cls):
        return {ConnectionStatus: {ConnectionStatus.connecting}}


class OnlineButton(LazyAdaptiveProxyRouter):
    @classmethod
    def defined_context(cls):
        return add_to_context(super(LazyAdaptiveProxyRouter,
                                    cls).defined_context(),
                              ConnectionStatus,
                              {ConnectionStatus.disconnecting})

    @classmethod
    def composite_monitors_provider(cls):
        return manager

    @classmethod
    def substitution_candidates(cls, arguments_provider=None):
        monitors_provider = cls.composite_monitors_provider()
        return {
            SendButton: lambda: SendButton(monitors_provider),
            PlaceRequestButton: lambda: PlaceRequestButton(monitors_provider)
        }

    def _choose_substitute(self):
        connection_status = self.composite_monitors_provider()\
            .request_monitor(ConnectionStatus).value()

        valid_substitutes = filter_substitutes(
            self.substitution_candidates().keys(),
            {
                ConnectionStatus: connection_status
            })

        return valid_substitutes.pop() \
            if len(valid_substitutes) == 1 else None

    def _route_to_substitute(self, old, new):
        pass


class SomeButton(LazyAdaptiveProxyRouter):
    @classmethod
    def defined_context(cls):
        return LazyAdaptiveProxyRouter.defined_context().update(
            {ConnectionStatus.disconnecting}
        )

    @classmethod
    def substitution_candidates(cls, arguments_provider=None):
        monitors_provider = cls.composite_monitors_provider()
        return {
            SaveButton: lambda: SaveButton(monitors_provider),
            OnlineButton: lambda: OnlineButton(monitors_provider)
        }

    def monitors_provider(self):
        return manager

    def _choose_substitute(self):
        connection_status = self.composite_monitors_provider()\
            .request_monitor(ConnectionStatus).value()

        valid_substitutes = filter_substitutes(
            self.substitution_candidates().keys(),
            {
                ConnectionStatus: connection_status
            })

        return valid_substitutes.pop() \
            if len(valid_substitutes) == 1 else None

    def _route_to_substitute(self, old, new):
        pass

print("Hello")
onlineBtn = OnlineButton()
print("Defined context of online button: {}".format(onlineBtn.defined_context()))


